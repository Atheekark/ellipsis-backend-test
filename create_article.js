//insert record

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db("mydb");
    var myobj = {
        id: 2,
        title: "Anewarticle",
        author: "JaneSmith",
        summary: "Some	content...",
        url: "/article/2",
        createdAt: "2017-03-20"
    };
    dbo.collection("articles").insertOne(myobj, function (err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();
    });

    myobj = {
        id: 1,
        title: "Anewarticle",
        author: "JohnSmith",
        summary: "Some	content...",
        url: "/article/1",
        createdAt: "2017-03-20"
    };
    dbo.collection("articles").insertOne(myobj, function (err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();
    });
});