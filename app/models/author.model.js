const mongoose = require('mongoose');

const AuthorSchema = mongoose.Schema({
    id: integer,
    name: string
}, {
        timestamps: true  //option to automatically add two new fields - createdAt and updatedAt to the schema.
    });

module.exports = mongoose.model('author', AuthorSchema);