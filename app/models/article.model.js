const mongoose = require('mongoose');

const ArticleSchema = mongoose.Schema({
    id: integer,
    author_id: integer,
    title: string,
    url: string,
    content: text
}, {
        timestamps: true  //option to automatically add two new fields - createdAt and updatedAt to the schema.
    });

module.exports = mongoose.model('article', ArticleSchema);