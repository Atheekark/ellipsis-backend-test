const article = require('../models/article.model.js');
const author = require('../models/author.model.js');

// Create and Save a new article
exports.create = (req, res) => {

    // Validate request
    if(!req.body.content) {
        return res.status(400).send({
            message: "article content can not be empty"
        });
    }

    // Create a article
    const article = new article({
        id: req.body.id, 
        author_id: req.body.author_id,
        title: req.body.title,
        url: req.body.url,
        content: req.body.content
    });

    // Save article in the database
    article.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the article."
        });
    });
};

// Create and Save a new author
exports.createAuthor = (req, res) => {

    // Validate request
    if(!req.body.content) {
        return res.status(400).send({
            message: "author content can not be empty"
        });
    }

    // Create an author
    const author = new author({
        id: req.body.id , 
        name: req.body.name
    });

    // Save author in the database
    author.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the author."
        });
    });
};

// Retrieve and return all articles from the database.
exports.findAll = (req, res) => {
    article.find()
    .then(articles => {
        res.send(articles);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving articles."
        });
    });
};

// Find a single article with a articleId
exports.findOne = (req, res) => {
    article.findById(req.params.articleId)
    .then(article => {
        if(!article) {
            return res.status(404).send({
                message: "article not found with id " + req.params.articleId
            });            
        }
        res.send(article);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "article not found with id " + req.params.articleId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving article with id " + req.params.articleId
        });
    });
};

// Update a article identified by the articleId in the request
exports.update = (req, res) => {
// Validate Request
if(!req.body.content) {
    return res.status(400).send({
        message: "article content can not be empty"
    });
}

// Find article and update it with the request body
article.findByIdAndUpdate(req.params.articleId, {
    id: req.body.id , 
        author_id: req.body.author_id,
        title: req.body.title,
        url: req.body.url,
        content: req.body.content
}, {new: true})
.then(article => {
    if(!article) {
        return res.status(404).send({
            message: "article not found with id " + req.params.articleId
        });
    }
    res.send(article);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "article not found with id " + req.params.articleId
        });                
    }
    return res.status(500).send({
        message: "Error updating article with id " + req.params.articleId
    });
});
};

// Delete a article with the specified articleId in the request
exports.delete = (req, res) => {
    article.findByIdAndRemove(req.params.articleId)
    .then(article => {
        if(!article) {
            return res.status(404).send({
                message: "article not found with id " + req.params.articleId
            });
        }
        res.send({message: "article deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "article not found with id " + req.params.articleId
            });                
        }
        return res.status(500).send({
            message: "Could not delete article with id " + req.params.articleId
        });
    });
};